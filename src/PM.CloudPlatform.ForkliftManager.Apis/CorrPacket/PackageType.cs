﻿namespace PM.CloudPlatform.ForkliftManager.Apis.CorrPacket
{
    public enum PackageType
    {
        Heart = 0,
        Login = 1,
        Gps = 2,
        Alarm = 3
    }
}